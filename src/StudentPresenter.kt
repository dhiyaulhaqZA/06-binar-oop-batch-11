class StudentPresenter(private val callback: Callback) {

    private val studentList = mutableListOf<Student>()

    fun getStudentList() {
        passDataToCallback(studentList)
    }

    fun getSmartStudentList() {
        val smartStudents = studentList.filter { it.score >= 70 }
        passDataToCallback(smartStudents)
    }

    private fun passDataToCallback(students: List<Student>) {
        if (students.isNotEmpty()) {
            callback.onGetStudentListSuccess(students)
        } else {
            callback.onGetStudentListFailure("Student list is empty") }

    }

    fun getStudentCount() = studentList.size

    fun insertStudent(student: Student) {
        studentList.add(student)
    }
}

interface Callback {
    fun onGetStudentListSuccess(students: List<Student>)
    fun onGetStudentListFailure(errMsg: String)
}
