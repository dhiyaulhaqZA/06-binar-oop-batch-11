fun main(args: Array<String>) {

    val kucing = Kucing("Kuku", "Jantan")
    kucing.showInformation()
    kucing.bersuara()
    kucing.berjalan()

    val kelinci = Kelinci("Keke", "Betina")
    kelinci.showInformation()
    kelinci.bersuara()
    kelinci.berjalan()
    kelinci.menggerakanKuping()


    val studentPresenter = StudentPresenter(object : Callback {
        override fun onGetStudentListSuccess(students: List<Student>) {
            students.forEach {
                println("Nama : ${it.name}")
                println("Score : ${it.score}")
            }
        }

        override fun onGetStudentListFailure(errMsg: String) {
            println(errMsg)
        }
    })

    studentPresenter.getStudentList()

    studentPresenter.insertStudent(Student("Nama1", 50))
    studentPresenter.insertStudent(Student("Nama2", 60))
    studentPresenter.insertStudent(Student("Nama3", 70))
    studentPresenter.insertStudent(Student("Nama4", 80))
    studentPresenter.insertStudent(Student("Nama5", 90))

    studentPresenter.getStudentList()
    studentPresenter.getSmartStudentList()
}













