abstract class Hewan(val name: String, val gender: String) {

    abstract fun bersuara()
    abstract fun berjalan()

    fun showInformation() {
        println("Nama   : $name")
        println("Gender : $gender")
    }
}

class Kucing(name: String, gender: String) : Hewan(name, gender) {

    override fun bersuara() {
        println("Meoong, meoong")
    }

    override fun berjalan() {
        println("Kucing berjalan")
    }
}

class Kelinci(name: String, gender: String) : Hewan(name, gender) {
    override fun bersuara() {
        println("Kutkutkut ")
    }

    override fun berjalan() {
        println("Kelinci melompat")
    }

    fun menggerakanKuping() {
        println("Kelinci menggerakan kuping")
    }
}